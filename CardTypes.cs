﻿using System;
namespace GroovyGoobersGame
{
    public enum CardTypes
    {
        LOOT,
        PUZZLE,
        REPAIR,
        ENCOUNTER,
        BLANK
    }
}
