﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace GroovyGoobersGameV2
{

    public class Card
    {

        public CardTypes CardType { get; set; }
        public string CardTitle { get; set; }
        public string Description { get; set; }

        Loot loot = new Loot();
        Repair repair = new Repair();
        Encounter encounter = new Encounter();
        Puzzle puzzle = new Puzzle();

        bool cardMade = false;

        int lootIndex = 0;
        int encountersIndex = 0;
        int repairIndex = 0;
        int puzzleIndex = 0;

        Random r = new Random();

        public Card()
        {
            CardType = CardTypes.BLANK;
            CardTitle = "";
            Description = "";
        }

        public Card(CardTypes type, string cardTitle, string description)
        {
            CardType = type;
            CardTitle = cardTitle;
            Description = description;
        }

        public Card generateCard()
        {
            Card c = new Card();
            c.CardType = getRandomType();
            string[,] array;
            //depending on type get description and title from 
            while(cardMade == false)    //we dont want to reuse scenarios so after we have used all of one scenario dont make another 
            {                           //card of that scenario, try again 
                switch (c.CardType)
                {
                    case CardTypes.LOOT:
                        array = loot.getLoot();
                        array[lootIndex, 0] = c.CardTitle;
                        array[lootIndex, 1] = c.Description;
                        lootIndex++;
                        cardMade = true; 
                        break;

                    case CardTypes.ENCOUNTER:
                        //starts with easy encounters and then we use more difficult, save boss battle for later
                        if (encountersIndex < 3)
                        {
                            array = encounter.getEncounterEasy();
                            array[encountersIndex, 0] = c.CardTitle;
                            array[encountersIndex, 1] = c.Description;
                            encountersIndex++;

                        }
                        else if (encountersIndex > 2 && encountersIndex < 6)
                        {
                            array = encounter.getEncounterMedium();
                            array[encountersIndex - 3, 0] = c.CardTitle;  //subtracts the easy encounters amount to get the 0 and 1 index
                            array[encountersIndex - 3, 1] = c.Description;
                            encountersIndex++;
                        }
                        else
                        {
                            array = encounter.getEncounterHard();
                            array[encountersIndex - 6, 0] = c.CardTitle;  //subtracts the easy + medium encounters amount to get the 0 and 1 index
                            array[encountersIndex - 6, 1] = c.Description;
                            encountersIndex++;
                        }
                        cardMade = true;
                        break;

                    case CardTypes.BLANK:
                        c.CardTitle = "Nothing to see here...";
                        c.Description = "Keep it moving...better things ahead...";
                        cardMade = true;
                        break;

                    case CardTypes.REPAIR:
                        array = repair.getRepairs();
                        array[repairIndex, 0] = c.CardTitle;
                        array[repairIndex, 1] = c.Description;
                        repairIndex++;
                        cardMade = true;
                        break;

                    case CardTypes.PUZZLE:
                        array = puzzle.getPuzzle();
                        array[puzzleIndex, 0] = c.CardTitle;
                        array[puzzleIndex, 1] = c.Description;
                        puzzleIndex++;
                        cardMade = true;
                        break;

                    default:

                        break;

                }

            }
            cardMade = false;

            return c;
        }

        public CardTypes getRandomType()
        {
            CardTypes[] values = { CardTypes.BLANK, CardTypes.ENCOUNTER, CardTypes.LOOT, CardTypes.PUZZLE, CardTypes.REPAIR };
            int type = r.Next(values.Length);
            return values[type];
        }
    }
}
