﻿using System;
//cool teehee 
//yo yo  
namespace GroovyGoobersGameV2
{
    public class Tile
    {
        public Card CurrentCard { get; set; }

        public int[] Coordinates { get; set; }

        public string PicturePath { get; set; }

        Card c = new Card(); 

        public Tile()
        {
            CurrentCard = new Card();
            Coordinates = new int[0];
            PicturePath = "";
        }

        public Tile(Card c, int[] coord, string s)
        {
            CurrentCard = c;
            Coordinates = coord;
            PicturePath = s;
        }

        public Tile generateTile()
        {
            Tile t = new Tile();
            t.CurrentCard = c.generateCard();
            return t;

        }

    }
}
