﻿
using System;
using System.IO;
using System.Linq;
//hello 
namespace GroovyGoobersGameV2
{
	/// <summary>
	/// Summary description for Class1
	/// </summary>
	public class Board
	{
		//holds the files for the pictures 
		public string[] PictureFiles { get; set; }

		public string[] PictureNames { get; set; }

		public Tile[,] TileList { get; set; }

		public int MyProperty { get; set; }

		Tile tile = new Tile();

		public Board()
		{
			PictureFiles = new string[0];   
			TileList = new Tile[4, 4];

		}

		public Board(string[] files, Tile[,] tl)
		{
			PictureFiles = files;
			TileList = tl;
		}

	
		public void generateBoard()
        {
			for(int i = 0; i < 4; i++)
            {
				for(int j = 0; j < 4; j++)
                {
					Tile t = new Tile();
					t = tile.generateTile();
                }
            }
        }

		public void getPictures(CardTypes type)
		{
			PictureFiles = Directory.GetFiles(@"..\..\Images");
			PictureNames = PictureFiles.Select(f => Path.GetFileName(f)).ToArray();
		}

	}
}
